//
//  YoutubeCell.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 17.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import UIKit

class YoutubeCell: UICollectionViewCell {
    @IBOutlet weak var thumbPhoto: UIImageView!
    @IBOutlet weak var titleLbl: UITextView!

}
