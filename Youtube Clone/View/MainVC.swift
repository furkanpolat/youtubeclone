//
//  ViewController.swift
//  Youtube Clone
//
//  Created by Furkan Polat Acikgoz on 16.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import UIKit
import LBTAComponents
import RevealingSplashView
class MainVC: UIViewController,UICollectionViewDelegate {
    
    //UI Declarations
    @IBOutlet weak var waitLbl: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var expandBtn: UIButton!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mainCV: UICollectionView!
    @IBOutlet weak var largeViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var splashView: YoutubePath!
    
    //Variable Declarations
    let youtubeHelper = YoutubeAPIServiceHelper.sharedInstance
    let vmHelper = ContentViewModel.shared
    let dataSource = TrendDataSource()
    let iconPath = YoutubePath()
    let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
    var lastContentOffset: CGFloat = 135
    var paddingSize:CGFloat = 0
    var isPaging:Bool = false
    var currentVideo = Content()
    var isSeaching:Bool = false
    var isScrolling:Bool = false
    var isPortrait:Bool = true
    var cellSize: CGSize = CGSize.zero

    lazy var viewModel:ContentViewModel = {
        let viewModel = ContentViewModel(content: dataSource)
        return viewModel
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        splashView.isHidden = false
        splashView = iconPath
        splashView.isHidden = true
        fetchDataFromAPI()
        self.searchBar.showsCancelButton = true
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        swipeGestureDelegate()
        expandBtn.imageView?.contentMode = .scaleAspectFit
        isPortrait = UIApplication.shared.statusBarOrientation.isPortrait
        self.mainCV.dataSource = self.dataSource
        self.viewModel.fetchTrends()
        
    }
  

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        
        if isPortrait{
            cellSize = CGSize(width: UIScreen.main.bounds.width * 0.45, height:UIScreen.main.bounds.width * 0.60)
        } else{
            cellSize = CGSize(width: UIScreen.main.bounds.width * 0.3, height:UIScreen.main.bounds.width * 0.4)
        }
        
        mainCV.collectionViewLayout.invalidateLayout()
        print("Orientation:",isPortrait,"Raw Value",  UIScreen.main.bounds.width)
        
    }
    
    //Fetch data from API and fill Response Array
    fileprivate func fetchDataFromAPI() {
        DispatchQueue.main.async {
            ContentViewModel.shared.fetchTrends(isPaging: false)
            self.mainCV.reloadData()
            self.waitLbl.isHidden = true
        }
    }
    
    
 
}

