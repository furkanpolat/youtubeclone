//
//  DetailsVC.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 23.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class DetailsVC: UIViewController {
  
    @IBOutlet weak var descLbl: UITextView!
    @IBOutlet weak var playerView: WKYTPlayerView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var mainView: UIView!
    
    var videoContent = Content()
    let rectShape = CAShapeLayer()
    var fileViewOrigin: CGPoint!
    var previousPosition:CGPoint!
    var transY:CGFloat!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = videoContent.title
        descLbl.text = videoContent.description
        playerView.load(withVideoId: videoContent.videoID)
        rectShape.bounds = self.playerView.frame
        rectShape.position = self.playerView.center
        rectShape.path = UIBezierPath(roundedRect: self.playerView.bounds, byRoundingCorners: [.bottomLeft , .bottomRight ], cornerRadii: CGSize(width: 40  , height: 40)).cgPath
        self.playerView.layer.mask = rectShape
        self.playerView.layer.backgroundColor = UIColor.clear.cgColor
        self.playerView.layer.shadowOpacity = 0.7
        self.playerView.layer.shadowRadius = 5
        self.playerView.layer.shadowOffset = CGSize(width: 1.5, height: 0.3)
        self.playerView.layer.shadowColor = UIColor.black.cgColor
        self.descLbl.contentOffset = CGPoint(x: 0, y: 0)
        fileViewOrigin = mainView.frame.origin
        previousPosition = mainView.frame.origin
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handledPan))
        mainView.addGestureRecognizer(panGesture)
        transY = 0.0
        
        
    }
    
   
    
    @objc func handledPan(_ sender: UIPanGestureRecognizer) {
        let view = sender.view!
        let translation = sender.translation(in: view)
        switch sender.state {
        case .began:
            
            previousPosition = view.center
           
            transY = translation.y
            break
         case .changed:
             view.center = CGPoint(x: view.center.x , y: view.center.y + translation.y)
             sender.setTranslation(CGPoint.zero, in: view)

            break
        case .ended:
            print(previousPosition , view.center)

            if view.center.y  - previousPosition.y > 150{
                dismiss(animated: true)
            }else{
                UIView.animate(withDuration: 0.3){
                    view.center = self.previousPosition
                }
            }
            break
        default:
            break
            
        }
    }
    
 
 
}
