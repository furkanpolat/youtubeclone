//
//  YoutubePath.swift
//  BezierTest
//
//  Created by Furkan P. Acikgoz on 2.08.2019.
//  Copyright © 2019 Furkan P. Acikgoz. All rights reserved.
//

import UIKit

class YoutubePath: UIView {
    public var path = UIBezierPath()
    override func draw(_ rect: CGRect) {
        let width = 200
        let height = 150
        let roundedRectangle = UIBezierPath(roundedRect: CGRect(x:Int(self.bounds.midX)-width/2-25, y: Int(self.bounds.midY)-height/2-50, width: width, height: height), cornerRadius: 30.0)
        UIColor.red.setFill()
        roundedRectangle.fill()
        self.createTriangle()
        UIColor.white.setFill()
        path.fill()
        UIView.animate(withDuration: 2.0){
            self.alpha = 0
            self.frame.origin.y -= 500
        }
        
    }
    func createTriangle() {
        let width = 200
        let height = 150
        path = UIBezierPath()
        path.move(to: CGPoint(x: Int(self.bounds.midX)-width/2+60, y:Int(self.bounds.midY)-height/2))
        path.addLine(to: CGPoint(x:Int(self.bounds.midX)-width/2+110, y: Int(self.bounds.midY)-height/2+30))
        path.addLine(to: CGPoint(x:Int(self.bounds.midX)-width/2+60, y: Int(self.bounds.midY)-height/2+60))
        path.close()
    }
    

}

