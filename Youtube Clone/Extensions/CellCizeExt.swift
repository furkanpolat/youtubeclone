//
//  CellCizeExt.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 17.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
import UIKit
extension MainVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isPortrait {
            return  CGSize(width: UIScreen.main.bounds.width  * 0.45, height:UIScreen.main.bounds.width * 0.60)
        }else{
            return  CGSize(width: UIScreen.main.bounds.width * 0.3, height:UIScreen.main.bounds.width * 0.4)
        }
        
        
    }
   
}
