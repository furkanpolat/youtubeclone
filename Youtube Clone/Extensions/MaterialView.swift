//
//  MaterialView.swift
//
//  Created by Furkan P. Açıkgöz on 8/16/16.
//  Copyright © 2016 Furkan P. Açıkgöz All rights reserved.
//

import UIKit

private var materialKey = false
private var shaColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0)
extension UIView {

    @IBInspectable var materialDesign: Bool {
        
        get {
            
            return materialKey
        }
        
        set {
            
            materialKey = newValue
            
            if materialKey {
                
                self.layer.masksToBounds = false
                self.layer.cornerRadius = 3.0
                self.layer.shadowOpacity = 0.8
                self.layer.shadowRadius = 5.0
                self.layer.shadowOffset = CGSize(width: 2.0, height: 3.0)
               // self.layer.shadowColor = shaColor.cgColor
                
            } else {
                
                self.layer.cornerRadius = 0
                self.layer.shadowOpacity = 0
                self.layer.shadowRadius = 0
                self.layer.shadowColor = nil
            }
            
        }
       
    }
    @IBInspectable var shadowColor:UIColor{
        get{
            return shaColor
        }set{
            shaColor = newValue
            self.layer.shadowColor = shaColor.cgColor
            
        }
    }

}
