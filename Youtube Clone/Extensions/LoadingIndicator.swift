//
//  LoadingIndicator.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 18.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
import UIKit
class LoadingIndicator:UIViewController{
    
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        var indicator:UIActivityIndicatorView{
                let indicate = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                indicate.hidesWhenStopped = true
                indicate.style = UIActivityIndicatorView.Style.gray
                return indicate
        }
    
    func beginLoading(){
        
        indicator.startAnimating();
        alert.view.addSubview(indicator)
        present(alert, animated: true, completion: nil)
        
    }
    
    func finishLoading(){
        
        dismiss(animated: false, completion: nil)
        
    }
}
