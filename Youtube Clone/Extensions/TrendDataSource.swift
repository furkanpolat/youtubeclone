//
//  TrendDataSoruce.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 1.08.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
import UIKit
class TrendDataSource : GenericDataSource<Content>, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return data.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! YoutubeCell
        cell.thumbPhoto.downloaded(from:self.data.value[indexPath.row].thumbnailPhoto)
        cell.titleLbl.text = self.data.value[indexPath.row].title
        cell.titleLbl.textContainer.lineBreakMode = .byTruncatingTail;
        
        
        return cell
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
  
}
