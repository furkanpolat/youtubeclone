//
//  SwipeToHeaderDelegate.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 23.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
import UIKit

extension MainVC{
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.displayHeader(scrollView)
    }
    
    func displayHeader(_ scrollView: UIScrollView) {
        if (self.lastContentOffset >= scrollView.contentOffset.y && self.lastContentOffset >= 0) {
            print("Move Up \(self.lastContentOffset - scrollView.contentOffset.y)")
            if paddingSize < 135 {
                
                if( largeViewTopConstraint.constant + self.lastContentOffset - scrollView.contentOffset.y >= 0){
                    largeViewTopConstraint.constant = 0
                    paddingSize = 135
                }else{
                    largeViewTopConstraint.constant += self.lastContentOffset - scrollView.contentOffset.y
                    paddingSize += self.lastContentOffset - scrollView.contentOffset.y
                }
                print(largeViewTopConstraint.constant )
            }
            print(self.lastContentOffset)

        }
        else if (self.lastContentOffset <= scrollView.contentOffset.y  && self.lastContentOffset >= 0 ) {
            print("Move Down \(self.lastContentOffset - scrollView.contentOffset.y)",mainView.frame.origin.y )
            if paddingSize  > 0 {
                if( largeViewTopConstraint.constant + self.lastContentOffset - scrollView.contentOffset.y <= -135){
                    largeViewTopConstraint.constant = -135
                    paddingSize = 0
                    
                }else{
                    largeViewTopConstraint.constant += self.lastContentOffset - scrollView.contentOffset.y
                    paddingSize += self.lastContentOffset - scrollView.contentOffset.y
                }
                
                print(paddingSize)
                print(self.lastContentOffset)
                
            }
        }
        
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
}
