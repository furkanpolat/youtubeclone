//
//  MainCVDelegate.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 23.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
import UIKit
extension MainVC{
    

   
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print("Displayed:",indexPath.row,"Total count: ",(viewModel.content?.data.value.count ?? 0))
        if indexPath.row == (viewModel.content?.data.value.count ?? 0) - 1{
            isPaging = true
            DispatchQueue.main.async {
                self.beginLoading()
                if !self.isSeaching{
                    self.viewModel.fetchTrends(isPaging: true)
                }else{
                    self.viewModel.fetchSearches(isPaging: true, searched: self.searchBar.text!)
                }
                self.mainCV.reloadData()
                self.finishLoading()
            }
            
        }else if (indexPath.row > (viewModel.content?.data.value.count ?? 0) - 22 && isPaging ) || !isPaging{
            let rotationTransformLeft = CATransform3DTranslate(CATransform3DIdentity, 20, 50, 0)
            let rotationTransformRight = CATransform3DTranslate(CATransform3DIdentity, -20, 50, 0)
            
            if indexPath.item % 2 == 0 {
                cell.layer.transform = rotationTransformLeft
                
            }else{
                cell.layer.transform = rotationTransformRight
            }
            
            cell.alpha = 0
            UIView.animate(withDuration: 0.55) {
                cell.layer.transform = CATransform3DIdentity
                cell.alpha = 1.0
            }
            isPaging = false
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        mainCV.collectionViewLayout.invalidateLayout()
        print(UIScreen.main.bounds.width)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        currentVideo = (viewModel.content?.data.value[indexPath.item])!
        performSegue(withIdentifier: "detailSegue", sender: nil)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue"{
            let vc = segue.destination as! DetailsVC
            vc.videoContent = currentVideo
        }
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isScrolling = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isScrolling = false

    }
   
}
