//
//  BezierExtension.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 5.08.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
import UIKit


extension UIBezierPath {
    func shapeImage() -> UIImage! {
        
        UIGraphicsBeginImageContextWithOptions(UIScreen.main.bounds.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: -(bounds.origin.x - self.lineWidth), y: -(bounds.origin.y - self.lineWidth))
        UIColor.black.setStroke()
        stroke()
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
