//
//  SearchBar.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 24.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
import UIKit


extension MainVC:UISearchBarDelegate{
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searched = searchBar.text{
            viewModel.fetchSearches(searched: searched)
            mainCV.reloadData()
            view.endEditing(true)
            isSeaching = true
            mainCV.contentOffset.y = 0
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.fetchTrends()
        mainCV.reloadData()
        view.endEditing(true)
        isSeaching = false
        searchBar.text = ""
        mainCV.contentOffset.y = 0

        
        
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" && isSeaching{
            viewModel.fetchTrends()
            mainCV.reloadData()
            view.endEditing(true)
            isSeaching = false
            mainCV.contentOffset.y = 0
        }
    }
}
