//
//  HeaderSwipeRecognizerDelegate.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 23.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
import UIKit
extension MainVC{
    
    @IBAction func expandBtn(_ sender: Any) {
        if largeViewTopConstraint.constant == 0 {
//            swipeDown()
        }
    }
    
    @IBAction func expandHeader(sender: UIGestureRecognizer) {
//        if let sendr = sender as? UISwipeGestureRecognizer{
//            if sendr.direction == .down && self.searchBar.alpha == 0 && largeViewTopConstraint.constant == 0 && !isScrolling{
//                swipeDown()
//            }else if sendr.direction == .up && self.searchBar.alpha == 1 && largeViewTopConstraint.constant == 0 && !isScrolling{
//                swipeUp()
//            }
//        }
//
    }
    func swipeDown(){
        print("swiped")
        self.searchBar.frame.origin.y -= 180

        UIView.animate(withDuration: 0.7){
            self.expandBtn.alpha = 0
            self.searchBar.alpha = 1
            self.searchBar.frame.origin.y += 180

            
        }
            searchBar.becomeFirstResponder()
        
    }
    func swipeUp(){
        print("swipedUP")
        
        UIView.animate(withDuration: 0.7){
            self.expandBtn.alpha = 1
            self.searchBar.alpha = 0
        }

        view.endEditing(true)
    }
    func swipeGestureDelegate() {
        let swipeDwn = UISwipeGestureRecognizer(target: self, action: #selector(expandHeader(sender:)))
        swipeDwn.direction = .down
        headerView.addGestureRecognizer(swipeDwn)
        let swipeUped = UISwipeGestureRecognizer(target: self, action: #selector(expandHeader(sender:)))
        swipeUped.direction = .up
        headerView.addGestureRecognizer(swipeUped)
    }
    

}
