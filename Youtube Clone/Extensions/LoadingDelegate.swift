//
//  LoadingDelegate.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 23.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
extension MainVC{
    func beginLoading(){
        loadingIndicator.startAnimating();
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func finishLoading(){
        
        dismiss(animated: false, completion: nil)
        
    }
}
