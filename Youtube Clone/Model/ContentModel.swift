//
//  ContentModel.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 29.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
public struct ContentModel:Decodable {
    var nextPageToken:String
    var items:[Items]
    struct Items : Codable{
        var snippet : Snippet
        var id : String
    }
    struct Snippet : Codable {
        var title:String                    //snippet -> title
        var description:String              //snippet -> description
        var thumbnails:Thumbnail
    }
    struct Thumbnail: Codable {
        var `default`: Default
        
    }
    struct Default : Codable {
        var url:String               //snippet -> thumbnails -> default -> url
    }
    init(nextPageToken:String? = nil,items:[Items] = Array()){
        self.nextPageToken = nextPageToken ?? ""
        self.items = items
    }
}
