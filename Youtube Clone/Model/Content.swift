//
//  GenericModel.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 1.08.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
public struct Content{

        var videoID:String                  //id -> videoId
        var thumbnailPhoto:String        //snippet -> thumbnails -> default -> url
        var title:String                 //snippet -> title
        var description:String            //snippet -> description
        init(){videoID = "";thumbnailPhoto="";title="";description=""} //Empty init

}
