//
//  YoutubeAPIServiceHelper.swift
//  Youtube Clone
//
//  Created by Furkan Polat Acikgoz on 16.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//
import UIKit
import Foundation
class YoutubeAPIServiceHelper{
    static let sharedInstance = YoutubeAPIServiceHelper()
    var searchData = ContentSearchModel()
    var trendData = ContentModel()
    var nextPageToken = ""
    func getResponseFromSearch(isPaging:Bool = false,searchValue:String) -> [Content]{
        var result = [Content]()
        var nsDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: "ConfigureAPI", ofType: "plist") {
            nsDictionary = NSDictionary(contentsOfFile: path)
        }
        let RAW_API = nsDictionary?["SEARCH_API"]!
        let API_KEY = nsDictionary?["API_KEY"]!
        var API_URL = "\(RAW_API as! String)\(searchValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)&key=\(API_KEY as! String)"
        print("API",API_URL)
        if isPaging{
            API_URL = "\(API_URL)&pageToken=\(nextPageToken)"
        }
        let url = URL(string: API_URL)!
        
    
        let semaphore = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: url){(data,response,err) in
            guard let data = data else {return}
            do{
               
                let decoder = JSONDecoder()
                self.searchData  = try decoder.decode(ContentSearchModel.self, from: data)
                result = self.setResult(isSearched: true)
                semaphore.signal()
            }catch let error{
                print("encoder failed",error)
            }
            }.resume()
        semaphore.wait()
        
        return result
    }
    func getResponseFromAPI(isPaging:Bool = false) -> [Content]{
        var result = [Content]()
        var nsDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: "ConfigureAPI", ofType: "plist") {
            nsDictionary = NSDictionary(contentsOfFile: path)
        }
        let RAW_API = nsDictionary?["RAW_API"]!
        let API_KEY = nsDictionary?["API_KEY"]!
        var API_URL = "\(RAW_API as! String)\(API_KEY as! String)"
        if isPaging{
            API_URL = "\(API_URL)&pageToken=\(nextPageToken)"
        }
        let url = URL(string: API_URL)!
        let semaphore = DispatchSemaphore(value: 0)

       URLSession.shared.dataTask(with: url){(data,response,err) in
        guard let data = data else {return}
            do{
                
                let decoder = JSONDecoder()
                 self.trendData  = try decoder.decode(ContentModel.self, from: data)
                result = self.setResult(isSearched: false)
                semaphore.signal()
            }catch let error{
                print("encoder failed",error)
            }
        }.resume()
        semaphore.wait()
        return result
    }
    
    fileprivate func setResult(isSearched:Bool)->[Content]{
        var content = [Content]()
        if isSearched {
            for i in 0..<self.searchData.items.count{
                  var tempContent = Content()
                tempContent.videoID = self.searchData.items[i].id.videoId
                tempContent.description = self.searchData.items[i].snippet.description
                tempContent.title = self.searchData.items[i].snippet.title
                tempContent.thumbnailPhoto = self.searchData.items[i].snippet.thumbnails.default.url
                self.nextPageToken=self.searchData.nextPageToken
                content.append(tempContent)
            }
        }else{
            for i in 0..<self.trendData.items.count{
                var tempContent = Content()
                tempContent.videoID = self.trendData.items[i].id
                tempContent.description = self.trendData.items[i].snippet.description
                tempContent.title = self.trendData.items[i].snippet.title
                tempContent.thumbnailPhoto = self.trendData.items[i].snippet.thumbnails.default.url
                self.nextPageToken=self.trendData.nextPageToken
                content.append(tempContent)
            }
            
        }
        return content
        
    }
}
