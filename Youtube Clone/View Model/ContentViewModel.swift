//
//  ContentViewModel.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 29.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
class ContentViewModel{
    let helper = YoutubeAPIServiceHelper.sharedInstance
    static let shared = ContentViewModel()
    weak var content:GenericDataSource<Content>?
    
    
    init(){}
    
    
    init(content:GenericDataSource<Content>?) {
        self.content = content
    }
  
    
    func fetchTrends(isPaging:Bool = false){
        let result = helper.getResponseFromAPI(isPaging: isPaging)
        if !isPaging{
            content?.data.value = [Content]()
        }
        for contentResult in result{
            content?.data.value.append(contentResult)
        }
    }
    func fetchSearches(isPaging:Bool = false,searched:String){
        let result = helper.getResponseFromSearch(isPaging: isPaging, searchValue: searched)
        if !isPaging{
            content?.data.value = [Content]()
        }
        for contentResult in result{
            content?.data.value.append(contentResult)
        }
    }
    
    
    
}
