//
//  GenericDataSource.swift
//  Youtube Clone
//
//  Created by Furkan P. Acikgoz on 31.07.2019.
//  Copyright © 2019 Furkan Polat Acikgoz. All rights reserved.
//

import Foundation
class GenericDataSource<T> : NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}
